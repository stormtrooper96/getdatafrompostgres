# -*- coding: utf-8 -*-

import os
import logs_messages  as lm

__author__ = 'Jonathan Pardo'

lm.print_log(lm.clean_folders)

# Clean folder
path = '{folder}/Dump'.format(folder=os.getcwd())
if os.path.exists(path):
    for file in os.listdir(path):
        os.remove(os.path.join(path, file))
    os.removedirs('{folder}/Dump'.format(folder=os.getcwd()))
lm.print_log(lm.finish)
