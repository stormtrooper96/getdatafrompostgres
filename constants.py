# SQL path
SQL_DIR_PATH = './SQL'
# Config file
CONFIG_FILE = './.config'
# Bonos file
bonos = 'bonos.sql'
# Cupones file
cupones = 'cupones.sql'
# Daviplata file
daviplata = 'daviplata.sql'
# driver logs file
driver_logs = 'driver_credits.sql'
# pay file
pay = 'pay.sql'
saldo='saldo.sql'
# Vales file
vales = 'vales.sql'
# send email config
server_user = 'server_user'
server_pass = 'server_pass'
recipients = 'recipients'
