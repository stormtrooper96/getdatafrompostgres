# -*- coding: utf-8 -*-
import os
import sys
from pandas import ExcelWriter
import constants as cts
import logs_messages as lm
from connections import get_data
import subprocess

lm.print_log(lm.start)


def config_project():

    query_file = os.path.join(os.path.abspath('.'), os.path.join(cts.SQL_DIR_PATH, cts.bonos))
    if not os.path.exists(query_file):
        print "The configuration file is missing"
        sys.exit(1)

# Create dump folder
if not os.path.exists(os.path.join(os.path.abspath('.'), "Dump")):
    os.makedirs(os.path.join(os.path.abspath('.'), "Dump"))

lm.print_log(lm.start_taskes)
taskes = [cts.cupones,cts.pay,cts.daviplata,cts.vales,cts.driver_logs,cts.bonos]
for task in taskes:
    print 'Execute ' + task
    query_path = os.path.join(cts.SQL_DIR_PATH, task)
    query = open(query_path, 'r')
    query = query.read()
    data = get_data(cts.CONFIG_FILE, query)
    df=data
    writer = ExcelWriter('{folder}/Dump/{file}.xlsx'.format(file=task[:task.index('.')], folder=os.getcwd()))
    df.to_excel(writer, 'bd', encoding='utf-8', index=False)
    ab= task[:task.index('.')]
    lm.print_log(lm.save_excel)
    writer.save()
lm.print_log(lm.create_compress)
subprocess.call('tar -zcvf Monthly_financial_reports.tar.gz Dump && mv Monthly_financial_reports.tar.gz Dump/', shell=True)
