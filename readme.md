# Send monthly financial reports

This project contains the monthly reports necessary for the financial area which must be sent on the first day of each month.
The goal of this project is to automate this process, the project is developed in Python 2.7.

### Installing

# Create virtualenv

```
mkvirtualenv email_finance
````

# install modules in virtualenv

```
workon email_finance
pip install -r ~/data_bi/Monthly_financial_reports/requirements.txt
```


## Authors

Jonathan Pardo



