import datetime as dt

start = 'Start process to send Monthly financial reports'
save_excel = 'create excel file '
start_taskes = 'Start to read queries'
create_compress = 'Start compress files in dump files'
send_files = 'Send mail with attach'
clean_folders = 'Start write process to clean folders'
finish = 'Finish process to build.'


def print_log(message, dic_format={}):
    dic_format.update({'time': dt.datetime.now().replace(microsecond=0)})
    message = '{time}. ' + message
    message = message.format(**dic_format)
    print message
