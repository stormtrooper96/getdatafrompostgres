# -*- coding: utf-8 -*-
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import constants as cts
from connections import parser_connection
import logs_messages as lm
import os

fromaddr = parser_connection(cts.CONFIG_FILE, cts.server_user)['fromaddr']
acces_token = parser_connection(cts.CONFIG_FILE, cts.server_pass)['pass']
recipients = parser_connection(cts.CONFIG_FILE, cts.recipients)['recipients']
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = recipients
msg['Subject'] = "Informes Financieros Mensuales"
body = "Buen día Envío la información del mes anterior \ Quedo atento "
msg.attach(MIMEText(body, 'plain'))
filename = "Monthly_financial_reports.tar.gz"
attachment = open( os.path.join(os.path.abspath('.'), "Dump",filename), "rb")
part = MIMEBase('application', 'octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
msg.attach(part)
server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(fromaddr,acces_token)
text = msg.as_string()
server.sendmail(fromaddr, recipients.split(','), text)
lm.print_log(lm.send_files)
server.quit()
