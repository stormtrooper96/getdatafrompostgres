from configparser import RawConfigParser
from pandas import io
from sqlalchemy import create_engine


def parser_connection(config_file, connection):
    config_obj = RawConfigParser()
    config_obj.read(config_file)
    return dict(config_obj.items(connection))


def create_conn_db(user, password, host, port, database):
    string_engine = 'postgresql://{username}:{password}@{host}:{port}/{database}'
    string_engine = string_engine.format(username=user,
                                         password=password,
                                         host=host,
                                         port=port,
                                         database=database)
    return create_engine(string_engine)


def get_db_from_file(config_file):
    config_dict = parser_connection(config_file, 'dwh')
    return create_conn_db(**config_dict)


def get_data(config_file, query):
    db = get_db_from_file(config_file)
    db.connect()
    return io.sql.read_sql_query(query, db)
